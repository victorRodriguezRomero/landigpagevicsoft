﻿namespace AppVicsoft.Services
{
    public class ServiceResult<T>: ServiceResult
    {
        public T Data { get; set; }
        public ServiceResult(bool isSuccessful, T data, string message = "")
           : base(isSuccessful, message)
        {
            Data = data;
        }

        public ServiceResult(bool isSuccessful, string message = "")
            : base(isSuccessful, message)
        {
        }
    }
    public class ServiceResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public ServiceResult(bool isSuccessful, string message = "")
        {
            Success = isSuccessful;
            Message = message;
        }
    }
}
