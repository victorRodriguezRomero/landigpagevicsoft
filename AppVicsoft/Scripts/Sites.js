﻿var contador_vision = 1;

document.getElementById('vision_more_inf').onclick = function obtener_info() {
    if (contador_vision == 1) {
        document.getElementById('texto_vinf').style.display = "block";
        document.getElementById('vision_more_inf').innerHTML = "Less Information";
        contador_vision++;
    } else {
        if (contador_vision == 2) {
            document.getElementById('texto_vinf').style.display = "none";
            document.getElementById('vision_more_inf').innerHTML = "More Information";
            contador_vision--;
        }
    }
}
var contador_mision = 1;
document.getElementById('mision_more_inf').onclick = function getinfo_mision() {
    if (contador_mision == 1) {
        document.getElementById('texto_minf').style.display = "block";
        document.getElementById('mision_more_inf').innerHTML = "Less Information";
        contador_mision++;
    } else {
        if (contador_mision == 2) {
            document.getElementById('texto_minf').style.display = "none";
            document.getElementById('mision_more_inf').innerHTML = "More Information";
            contador_mision--;
        }
    }
}

document.getElementById("enviar").onclick = function validar_correo() {
    var correo = document.getElementById("email").value;

    if (correo == "") {
        alert("Intruduzca un correo");
    } else {
        validar_email(correo);
    }
}

function validar_email(valor) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(valor)) {
        alert("La dirección de email " + valor + " es correcta." + " "+ "Le enviaremos mas información a su correo");
    } else {
        alert("La dirección de email es incorrecta.");
    }
}