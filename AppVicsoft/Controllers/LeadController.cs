﻿using AppVicsoft.Models.ViewModels;
using AppVicsoft.Services;
using System.Threading.Tasks;
using System.Web.Mvc;
using AppVicsoft.Models.Data;
using AppVicsoft.Models.Entities;


namespace AppVicsoft.Controllers
{
    public class LeadController:Controller
    {
        private readonly userService _user;
        readonly DatabdContext _context;

        public LeadController (userService userService, DatabdContext context)
        {
            _user = userService;
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult> Create(ContactViewModels model)
        {
            if (ModelState.IsValid)
            {
                Leads user = new Leads
                {
                    Name = model.name,
                    LastNameFather = model.fatherLastName,
                    LastNameMother = model.motherLastName,
                    descripcionSistemas = model.DescripcionSistemas,
                    MontoDisponible = model.montoDisponible,
                    Email = model.email,
                    ServicesType = model.typeService
                };
                _context.Leads.Add(user);
                _context.SaveChanges();

                return RedirectToAction("Index", "Home");

            }
            else {
                return View(model);
            }
           
        }
    }
}