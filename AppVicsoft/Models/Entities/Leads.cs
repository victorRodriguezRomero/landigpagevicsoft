﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace AppVicsoft.Models.Entities
{
    using System;
    public partial class Leads
    {
        [Required, Key]
        public int id { get; set; }

        [Required, MaxLength(45)]
        public string Name { get; set; }

        [Required, MaxLength(25)]
        public string LastNameFather { get; set; }

        [Required, MaxLength(25)]
        public string LastNameMother { get; set; }

        [Required, MaxLength(35)]
        public string Email { get; set; }

        [Required, MaxLength(25)]
        public string ServicesType { get; set; }

        [Required, MaxLength(20)]
        public string MontoDisponible { get; set; }

        [Required, MaxLength(75)]
        public string descripcionSistemas { get; set; }
    }
}