﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AppVicsoft.Models.ViewModels
{
    public class ContactViewModels
    {
        [Required, Key]
        public int Id { get; set; }

        [Required, MaxLength(45)]
         public string name { get; set; }

        [Required, MaxLength(25)]
         public string fatherLastName { get; set; }

        [Required, MaxLength(25)]
        public string motherLastName { get; set; }

        [Required, MaxLength(35), EmailAddress]
        public string email { get; set; }

        [Required, MaxLength(25)]
        public string typeService { get; set; }

        [Required, MaxLength(20)]
        public string montoDisponible { get; set; }

        [Required, MaxLength(75)]
        public string DescripcionSistemas { get; set; }



    }
}