﻿namespace AppVicsoft.Models.Data
{
    using AppVicsoft.Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DatabdContext : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'DatabdContext' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'AppVicsoft.Models.Data.DatabdContext' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'DatabdContext'  en el archivo de configuración de la aplicación.
        public DatabdContext()
            : base("name=DatabdContext")
        {
        }
        public virtual DbSet<Leads> Leads { get; set; }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}